# eselect-autostart

[Eselect](https://wiki.gentoo.org/wiki/Project:Eselect) module for managing symlinks to .desktop entries in `$XDG_CONFIG_HOME/autostart`.

## Usage

Unlike most other eselect modules, there's no need to
run this as root. All files are created in `$HOME` and doing so would pass different `$XDG_CONFIG_HOME` to the script.

To view available .desktop entries use
```
eselect autostart list
```
To enable starting target application on login use
```
eselect autostart enable <TARGET>
```
where `TARGET` is either a number from _list_ command
or entry name directly. Specifying .desktop suffix is not
necessary. Both of the following will work.
```
eselect autostart enable Alacritty
```
```
eselect autostart enable Alacritty.desktop
```
While specifying a number from _list_ command is unambiguous
using filenames is not and symlink will be created to
the first matching entry according to
[Desktop Application Autostart Specification](https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html).

To prevent an application from being started automatically
use
```
eselect autostart disable Alacritty
```

## Installation

Copy into eselect's module directory.
```
cp ./autostart.eselect /usr/share/eselect/modules/.
```
